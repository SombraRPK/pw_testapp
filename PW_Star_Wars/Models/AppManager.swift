//
//  AppManager.swift
//  PW_Star_Wars
//
//  Created by Reivaj Gómez Pérez on 11/02/21.
//

import UIKit

protocol AppManagerDelegate {
    func didLoadData(_ tilesLoaded: [TileStoredInfo])
    func didLoadImage(_ images: [Data?])
    func didFailWithError(_ error: String)
}

struct AppManager {
    var delegate: AppManagerDelegate?
    
    func fetchTiles() {
        let internetConnectionMonitor = DeviceConnectivity()
        let areTilesAlreadyStored = UserDefaults.standard.bool(forKey: AppConstants.dataPersistence.tilesAlreadyStoredId)
        if internetConnectionMonitor.isConnectedToInternet() {
            downloadTiles()
        } else {
            if areTilesAlreadyStored {
                getStoredTiles()
                delegate?.didFailWithError (AppConstants.errorMessages.noInternetWithStoredData)
            } else {
                delegate?.didFailWithError(AppConstants.errorMessages.noInternetWithNoStoredData)
            }
        }
    }
    
    private func downloadTiles(){
        
        if let url = URL(string: AppConstants.requestUrl) {
            let urlSession = URLSession(configuration: .default)
            let task = urlSession.dataTask(with: url) { (data, response, error) in
                if error != nil {
                    delegate?.didFailWithError(error!.localizedDescription)
                    return
                }
                if let secureData = data {
                    let jsonDecoder = JSONDecoder()
                    do {
                        let allTiles = try jsonDecoder.decode([TileWebInfo].self, from: secureData)
                        downloadTilesImages(tiles: allTiles)
                        storeDownloadedTiles(allTiles)
                        getStoredTiles()
                    } catch {
                        delegate?.didFailWithError(AppConstants.errorMessages.dataDecodingError)
                    }
                }
            }
            task.resume()
        } else {
            delegate?.didFailWithError(AppConstants.errorMessages.notValidURL)
        }
    }
    
    private func downloadTilesImages(tiles: [TileWebInfo]){
        var imagesData = [Data?](repeating: nil, count: tiles.count)
        for index in 0..<tiles.count {
            DispatchQueue.global().async {
                if let url = URL (string: tiles[index].image ?? "") {
                    if let data = try? Data(contentsOf: url) {
                        imagesData[index] = data
                        delegate?.didLoadImage(imagesData)
                    }
                }
            }
        }
    }
    
    private func getFormattedScheduleInfo (stringDate: String?) -> String {
        var finalStringDate = String()
        if let secureStringDate = stringDate, secureStringDate.isEmpty == false {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            let brokeDownDate = dateFormatter.date (from: secureStringDate)
            let calendar = Calendar.current
            let components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: brokeDownDate!)
            let timeString = getTimeString(components.hour!, components.minute!)
            finalStringDate = "\(AppConstants.monthName[components.month!]!) \(components.day!), \(components.year!) at \(timeString)"
        }
        return finalStringDate
    }
    
    private func getTimeString (_ hour: Int, _ minute: Int) -> String {
        var finalStringHour = String()
        var minutesString = "\(minute)"
        if minute < 10 {
            minutesString = "0\(minute)"
        }
        if hour > 12 {
            finalStringHour = "\(hour - 12):\(minutesString) pm"
        } else {
            finalStringHour = "\(hour):\(minutesString) am"
        }
        return finalStringHour
    }
    
    private func getStoredTiles() {
        if UserDefaults.standard.bool(forKey: AppConstants.dataPersistence.tilesAlreadyStoredId) {
            let storedDataTiles = UserDefaults.standard.data(forKey: AppConstants.dataPersistence.storedTilesId)
            do {
                let storedTiles = try JSONDecoder().decode([TileStoredInfo].self, from: storedDataTiles!)
                delegate?.didLoadData(storedTiles)
            } catch {
                delegate?.didFailWithError(AppConstants.errorMessages.unableGetStoredData)
            }
        }
    }
    
    private func storeDownloadedTiles (_ webTiles: [TileWebInfo]){
        let tilesToStore = formatWebTilesForStoring(webTiles)
        do {
            let formattedTilesToStore = try JSONEncoder().encode(tilesToStore)
            UserDefaults.standard.set(formattedTilesToStore, forKey: AppConstants.dataPersistence.storedTilesId)
            UserDefaults.standard.set(true, forKey: AppConstants.dataPersistence.tilesAlreadyStoredId)
            UserDefaults.standard.synchronize()
        } catch {
            delegate?.didFailWithError(AppConstants.errorMessages.unableStoreData)
        }
    }
    
    private func formatWebTilesForStoring (_ webTiles: [TileWebInfo]) -> [TileStoredInfo] {
        let tileToStoreSample = TileStoredInfo (title: String(), description: String(), schedule: String(), image: Data(), location: String())
        var tilesToStore = [TileStoredInfo] (repeating: tileToStoreSample, count: webTiles.count)
        if webTiles.indices.contains(0) {
            for index in 0..<webTiles.count {
                tilesToStore[index].image = nil
                tilesToStore[index].schedule = getFormattedScheduleInfo(stringDate: webTiles[index].date)
                tilesToStore[index].title = webTiles[index].title
                tilesToStore[index].location = "\(webTiles[index].locationline1), \(webTiles[index].locationline2)"
                tilesToStore[index].description = webTiles[index].description
            }
        }
        return tilesToStore
    }
}
