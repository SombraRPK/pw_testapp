//
//  Tile.swift
//  PW_Star_Wars
//
//  Created by Reivaj Gómez Pérez on 11/02/21.
//

import Foundation

///Contains the relevant params  of the  JSON to consume
struct TileWebInfo: Codable {
    let id: Int
    let description: String
    let title: String
    let timestamp: String?
    let date: String?
    let image: String?
    let locationline1: String
    let locationline2: String
}
