//
//  TileStoredInfo.swift
//  PW_Star_Wars
//
//  Created by Reivaj Gómez Pérez on 13/02/21.
//

import Foundation

///Contains the relevant params to be displayed in the tiles screens with the processed information. The tiles data is stored using this struct.
struct TileStoredInfo: Codable {
    var title: String
    var description: String
    var schedule: String?
    var image: Data?
    var location: String
}
