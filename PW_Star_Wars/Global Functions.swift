//
//  Functions.swift
//  PW_Star_Wars
//
//  Created by Reivaj Gómez Pérez on 12/02/21.
//

import UIKit
/// Applies the text color defined in AppConstant struct
func applyTextColor(to labels: [UILabel]) {
    for label in labels {
        label.textColor = AppConstants.colors.fontColor
    }
}
