//
//  TileCell.swift
//  PW_Star_Wars
//
//  Created by Reivaj Gómez Pérez on 11/02/21.
//

import UIKit

class TileCell: UITableViewCell {
    
    var appManager = AppManager()

    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var imageOpacityView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var abstractLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        // Fonts color
        applyTextColor(to: [dateLabel, titleLabel, locationLabel, abstractLabel])
        
        // Background image
        backgroundImageView.layer.cornerRadius = AppConstants.tileCell.layerCornerRadius
        imageOpacityView.backgroundColor = AppConstants.colors.tileImageOpacityColor
        imageOpacityView.layer.cornerRadius = AppConstants.tileCell.layerCornerRadius
        
        // Image shadow
        imageOpacityView.layer.shadowColor = AppConstants.colors.tileImageShadowColor.cgColor
        imageOpacityView.layer.shadowOpacity = AppConstants.tileCell.imageShadowOpacity
        imageOpacityView.layer.shadowOffset = .zero
        imageOpacityView.layer.shadowRadius = AppConstants.tileCell.imageShadowRadius
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
