//
//  DetailsViewController.swift
//  PW_Star_Wars
//
//  Created by Reivaj Gómez Pérez on 12/02/21.
//

import UIKit

class DetailsViewController: UIViewController {
    
    var appManager = AppManager()
    let gradient: CAGradientLayer = CAGradientLayer()

    @IBOutlet weak var tileImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var gradientView: GradientView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var tileBackgroundImage: UIImage?
    var tileDate = String()
    var tileTitle = String()
    var tileLocation = String()
    var tileContent = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.barTintColor = .white
    }

    func setupUI(){
        // Font color
        applyTextColor(to: [dateLabel, titleLabel, locationLabel])
        descriptionLabel.textColor = AppConstants.colors.fontColor
        
        // Image setup
        gradientView.backgroundColor = AppConstants.colors.tileImageOpacityColor
        tileImage.contentMode = .scaleAspectFill
        setupGradientView()
        
        // Filling with data
        tileImage.image = tileBackgroundImage
        dateLabel.text = tileDate
        titleLabel.text = tileTitle
        locationLabel.text = tileLocation
        descriptionLabel.text = tileContent
        
        // Navigation bar customization
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationController?.navigationBar.barTintColor = .black
        let shareBarButtonItem = UIBarButtonItem(image: UIImage(systemName: AppConstants.imageAssetsNames.shareIcon), style: .plain, target: self, action: #selector(shareButtonPressed))
        navigationItem.setRightBarButton(shareBarButtonItem, animated: true)
    }
    
    func setupGradientView() {
        gradientView.topColor = UIColor.clear
        gradientView.bottomColor = UIColor.black
        gradientView.updateViewGradient()
    }

    
    @objc func shareButtonPressed() {
        let activityController = UIActivityViewController (activityItems: [tileContent], applicationActivities: nil)
        self.present(activityController, animated: true)
        if let popOver = activityController.popoverPresentationController {  // This lets sharing on iPad
            popOver.sourceView = self.view
            popOver.sourceRect = CGRect (x: UIScreen.main.bounds.width - 40, y: 40, width: 20, height: 20)
        }
    }
}
