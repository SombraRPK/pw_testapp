//
//  ViewController.swift
//  PW_Star_Wars
//
//  Created by Reivaj Gómez Pérez on 11/02/21.
//

import UIKit

class TilesViewController: UIViewController {

    @IBOutlet weak var tilesTableView: UITableView!
    
    var appManager = AppManager()
    var tilesInfo = [TileStoredInfo]()
    var tilesImagesData = [Data?]()
    var isImagesDataAvailable = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tilesTableView.dataSource = self
        tilesTableView.delegate = self
        tilesTableView.register(UINib(nibName: AppConstants.tileCell.cellName, bundle: nil), forCellReuseIdentifier: AppConstants.tileCell.cellIdentifier)
        appManager.delegate = self
        appManager.fetchTiles()
        setupUI()
    }
    
    func setupUI() {
        title = AppConstants.appName
    }
}

// MARK: - TableView Data

extension TilesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tilesInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tileCell = tableView.dequeueReusableCell(withIdentifier: AppConstants.tileCell.cellIdentifier, for: indexPath) as! TileCell
        setupCell(tileCell, indexPath)
        return tileCell
    }
    
    func setupCell(_ tileCell: TileCell, _ indexPath: IndexPath){
        let row = indexPath.row
        if isImagesDataAvailable {
            if let imageData = tilesImagesData[row] {
                tileCell.backgroundImageView.image = UIImage (data: imageData)
            } else {
                tileCell.backgroundImageView.image = UIImage (named: AppConstants.imageAssetsNames.tileImagePlaceholder)
            }
        } else {
            tileCell.backgroundImageView.image = UIImage (named: AppConstants.imageAssetsNames.tileImagePlaceholder)
        }
        tileCell.backgroundImageView.contentMode = .scaleAspectFill
        tileCell.dateLabel.text = tilesInfo[row].schedule
        tileCell.titleLabel.text = tilesInfo[row].title
        tileCell.locationLabel.text = tilesInfo[row].location
        tileCell.abstractLabel.text = tilesInfo[row].description
    }
}

// MARK: - TableView Delegate
extension TilesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let detailViewController = storyboard?.instantiateViewController(withIdentifier: AppConstants.detailStoryboardId) as! DetailsViewController
        let selectedCell = tilesTableView.cellForRow(at: indexPath) as! TileCell
        
        detailViewController.tileBackgroundImage = selectedCell.backgroundImageView.image
        detailViewController.tileDate = tilesInfo[indexPath.row].schedule ?? ""
        detailViewController.tileTitle = tilesInfo[indexPath.row].title
        detailViewController.tileLocation = tilesInfo[indexPath.row].location
        detailViewController.tileContent = tilesInfo[indexPath.row].description
        
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}

// MARK: - AppManager Delegate

extension TilesViewController: AppManagerDelegate {
    
    func didLoadData(_ tilesLoaded: [TileStoredInfo]) {
        tilesInfo = tilesLoaded
        DispatchQueue.main.async {
            self.tilesTableView.reloadData()
        }
    }
    
    func didFailWithError(_ error: String) {
        DispatchQueue.main.async {
            let errorAlert = UIAlertController(title: AppConstants.alertsInformation.tilesVCAlert.alertTitle, message: error, preferredStyle: .alert)
            let alertActionDismiss = UIAlertAction (title: AppConstants.alertsInformation.tilesVCAlert.dismissActionTitle, style: .default, handler: nil)
            let alertActionTry = UIAlertAction (title: AppConstants.alertsInformation.tilesVCAlert.tryActionTitle, style: .default) { (_) in
                self.dismiss(animated: true, completion: nil)
                self.appManager.fetchTiles()
            }
            errorAlert.addAction(alertActionDismiss)
            errorAlert.addAction(alertActionTry)
            self.present(errorAlert, animated: true)
        }
    }
    
    func didLoadImage(_ imagesData: [Data?]) {
        isImagesDataAvailable = true
        tilesImagesData = imagesData
        DispatchQueue.main.async {
            self.tilesTableView.reloadData()
        }
    }
}
