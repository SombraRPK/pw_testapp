//
//  Constants.swift
//  PW_Star_Wars
//
//  Created by Reivaj Gómez Pérez on 11/02/21.
//

import UIKit

///Defines all code constants so it can be easily mantainable and
struct AppConstants {
    static let appName = "Star Wars Tiles"
    static let requestUrl = "https://raw.githubusercontent.com/phunware-services/dev-interview-homework/master/feed.json"
    static let detailStoryboardId = "detailVC"

    struct tileCell {
        static let cellIdentifier = "ReusableTileCell"
        static let cellName = "TileCell"
        static let layerCornerRadius: CGFloat = 15.0
        static let imageShadowRadius: CGFloat = 3.0
        static let imageShadowOpacity: Float = 0.8
    }
    
    struct colors {
        static let fontColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
        static let tileImageOpacityColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2507344429)
        static let tileImageShadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    
    static let monthName = [
        1:"January",
        2:"February",
        3:"March",
        4:"April",
        5:"May",
        6:"June",
        7:"July",
        8:"August",
        9:"September",
        10:"October",
        11:"November",
        12:"December"
    ]

    struct imageAssetsNames {
        static let tileImagePlaceholder = "ImagePlaceholder"
        static let shareIcon = "square.and.arrow.up.fill"
    }
    
    struct dataPersistence {
        static let storedTilesId = "storedTiles"
        static let tilesAlreadyStoredId = "tilesAlreadyStored"
    }
    
    struct errorMessages {
        static let noInternetWithStoredData = "It seems like there is no Internet connection. Downloaded tiles are showing, however, you need Internet connection so images can be shown."
        static let noInternetWithNoStoredData = "You have no Internet connection. Please check you have WiFi or cellular data with an active connection."
        static let unableGetStoredData = "Couldn't get stored data."
        static let unableStoreData = "Couldn't store data."
        static let dataDecodingError = "An error occurred when decoding the downloaded data."
        static let notValidURL = "URL error."
    }
    
    struct alertsInformation {
        struct tilesVCAlert {
            static let alertTitle = "Oops!"
            static let tryActionTitle = "Try again"
            static let dismissActionTitle = "Ok"
        }
    }
}
