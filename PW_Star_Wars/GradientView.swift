//
//  GradientView.swift
//  PW_Star_Wars
//
//  Created by Reivaj Gómez Pérez on 16/02/21.
//

import UIKit

/// Lets a UIVIew object render a two-color gradient
class GradientView: UIView {
    var topColor: UIColor = UIColor.clear
    var bottomColor: UIColor = UIColor.clear
    
    override class var layerClass: AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    
    func updateViewGradient() {
        let layer = self.layer as! CAGradientLayer
        layer.colors = [topColor.cgColor, bottomColor.cgColor]
    }
}
